package bo.com.fie.cognos.cursojava.componentes;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ValidationHandler implements SOAPHandler<SOAPMessageContext> {

	/**
	 * Se envia cuando todo esta bien en el mensaje. Si se devuelve true estaria
	 * correcto y sigue con el proceso. Si se devuelve false estaria INCORRECTO y no
	 * continua con el proceso.
	 */
	public boolean handleMessage(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleMessage");
		this.mostrarMensaje(context);
		this.validarDatos(context);
		// siempre debe ser true si esta todo correcto, caso contrario false para que no
		// continue
		return true;
	}

	/**
	 * Se envia cuando hay un problema en el mensaje.
	 */
	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleFault");
		return false;
	}

	/**
	 * Cierra los mensajes.
	 */
	public void close(MessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("close-handle");

	}

	/**
	 * Siempre se ejecuta.
	 */
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Con este metodo verificamos lo que tiene el mensaje.
	 */
	public void mostrarMensaje(SOAPMessageContext context) {
		try {
			boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			System.out.println("Mensaje de " + (isOutBound ? "Salida" : "Entrada") + "...");
			SOAPMessage soapMessage = context.getMessage();
			soapMessage.writeTo(System.out);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean validarDatos(SOAPMessageContext context) {
		boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (!isOutBound) {
			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			System.out.println("IP: " + request.getRemoteAddr());
			System.out.println("Host: " + request.getRemoteHost());
			System.out.println("Header-Usuario: " + request.getHeader("usuario"));
		}
		return !isOutBound;
	}
}
