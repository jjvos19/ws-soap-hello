package bo.com.fie.cognos.cursojava.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import bo.com.fie.cognos.cursojava.componentes.ManejadorPersonas;
import bo.com.fie.cognos.cursojava.entidades.Persona;

@WebService
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class HolaMundoDLService {

	@WebMethod
	//@WebResult(partName  = "saludo")
	public String saludar() {
		return "Hola mundo Document-Literal!!!";
	}

	@WebMethod
	@WebResult(name = "persona") // Cambia el nombre del resultado
	public Persona obtenerPersona(@WebParam(name = "nombre") String nombre, // da un nombre a los parametros en el wsdl
			@WebParam(name = "paterno") String apPaterno, @WebParam(name = "materno") String apMaterno,
			@WebParam(name = "edad") short edad) {
		Persona p = ManejadorPersonas.cargar(nombre, apPaterno, apMaterno, edad);
		return p;
	}

}
