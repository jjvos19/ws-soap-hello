package bo.com.fie.cognos.cursojava.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import bo.com.fie.cognos.cursojava.componentes.ManejadorPersonas;
import bo.com.fie.cognos.cursojava.entidades.Persona;

/*
 * TAREA
 * Crear un web services que permita la creacion de un 
 * listado de personas y un cliente que muestre las personas por consola.
 * Nombre apPaterno apMaterno Edad 
 */

@WebService
@HandlerChain(file = "handler-chain.xml")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class ClientesDLServices {

	@Resource
	WebServiceContext wsc;

	@WebMethod(operationName = "saludar")
	public String saludar() throws Exception  {
		MessageContext messageContext = wsc.getMessageContext();
		Map<String, Object> headers = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		List<String> usuario = (List<String>) headers.get("usuario");
		List<String> password = (List<String>) headers.get("password");
		String cadena;
		if (usuario != null && password != null && usuario.size() > 0 && password.size() > 0
				&& usuario.get(0).equals("jj") && password.get(0).equals("JVO")) {
			cadena = "Bienvenido JJ";
		} else {
			throw new Exception("Error en la autenticacion");
		}
		return cadena;
	}

	@WebMethod(operationName = "cargarPersonas")
	@WebResult(name = "listaPersonas")
	public List<Persona> generarPersonas(@WebParam(name = "personas") List<String> datos,
			@WebParam(name = "separador") String separador) {
		List<Persona> lista = new LinkedList<Persona>();
		for (String fila : datos) {
			String[] elemento = fila.split(separador);
			short d = 0;
			try {
				d = Short.parseShort(elemento[3].trim());
			} catch (NumberFormatException ex) {
				d = -1;
			}
			Persona p = ManejadorPersonas.cargar(elemento[0], elemento[1], elemento[2], d);
			lista.add(p);
		}
		return lista;
	}

	@WebMethod(operationName = "cargarPersonasR")
	@WebResult(name = "listaPersonas")
	public List<Persona> generarPersonasR(@WebParam(name = "cantidad") int cantidad) {
		List<Persona> lista = new LinkedList<Persona>();
		int limite = 0;
		if (cantidad > 0) {
			limite = cantidad;
		}
		for (int i = 0; i < limite; i++) {
			int indice = i + 1;
			Persona p = ManejadorPersonas.cargar("nombre" + indice, "paterno" + indice, "materno" + indice,
					(short) indice);
			lista.add(p);
		}
		return lista;
	}
}
