package bo.com.fie.cognos.cursojava.componentes;

import bo.com.fie.cognos.cursojava.entidades.Persona;

public class ManejadorPersonas {

	public static Persona cargar(String nombre, String apPaterno, String apMaterno, short edad) {
		Persona p = new Persona();
		p.setNombre(nombre);
		p.setApPaterno(apPaterno);
		p.setApMaterno(apMaterno);
		p.setEdad(edad);
		return p;
	}
	
	public static String toString(Persona persona) {
		String cadena = String.format("%s %s %s, %d", 
				persona.getNombre(), persona.getApPaterno(), persona.getApMaterno(), persona.getEdad());
		return cadena;
	}
}
