package bo.com.fie.cognos.cursojava.services;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import bo.com.fie.cognos.cursojava.componentes.ManejadorPersonas;
import bo.com.fie.cognos.cursojava.entidades.Persona;

@WebService
// Style: Indica como se va a formar el wsdl
@SOAPBinding(style = Style.RPC, use = Use.LITERAL)
//Tarea: Hacer cuatro proyecto donde se utilice:
// RPC/Literal
// RPC/Encode
// Document/Literal
// Document/Encode

// Tambien crear un paquete entity, donde exista una clase llamada 
// persona con los atributos: Nombre, ApPaterno, ApMaterno, Edad
// Dentro de esta clase adicionar un metodo llamadao cargarPersona(S, S, S, S)
public class HolaMundoService {

	@WebMethod
	//@WebResult(name = "saludo")
	public String saludar() {
		return "Hola mundo";
	}

	@WebMethod
	@WebResult(name = "persona") // Cambia el nombre del resultado
	// da un nombre a los parametros en el wsdl
	public Persona obtenerPersona(@WebParam(name = "nombre") String nombre, 
			@WebParam(name = "paterno") String apPaterno, @WebParam(name = "materno") String apMaterno,
			@WebParam(name = "edad") short edad) {
		Persona p = ManejadorPersonas.cargar(nombre, apPaterno, apMaterno, edad);
		return p;
	}
}
