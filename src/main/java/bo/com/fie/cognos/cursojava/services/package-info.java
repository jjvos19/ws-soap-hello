package bo.com.fie.cognos.cursojava.services;

/*
 
Style: Indica el estilo del wsdl. Sus obsiones son RPC o Document
<ul>
<li>RPC: Muestra toda la definicion en el wsdl.
<li>Document: Muestra un wsdl simplificado, mandando a otro archivo la defincion de los metodos y atributos.
</ul>



Use: Muestra la informacion de namespace o no segun el LITERAL o ENCODED.
NO es bueno utilizar a la fecha ENCODED.
<ul>
<li> LITERAL: Muestra toda la informacion (namespace) de la defincion
<li> ENCODED: Oculta parte de la informacion (namespace), NO ES RECOMENDADO UTILIZAR ESTO.
</ul>


 @WebService: Indica que la clase es un servicio.
 @WebMethod: Indica que el metodo es una operacion del servicio.
 @WebResult: Se utiliza para indicar el nombre del resultado de un metodo. 
 	@WebResult(name="nombreResultado")
 
 @WebParam: Se utiliza para nombrar a los parametros de un metodo.
 	@WebParam(name="NombreParametro")
 
 
 
 WSDL: Contiene
  Definitions: Es la raiz de wsdl, contiene el espacio de nombres del servicio.
  
  
  Binding
  Services y ports: Contiene la url que se va a consumir. El objectivo es saber donde se debe llegar.
*/